#!/usr/bin/env python
# -*-coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp import _

class sale_order(models.Model):
    _inherit = 'sale.order'

    credit_check = fields.Boolean('Credit check', default=False)
    delivery_method = fields.Selection([
        ('express', 'Express'), ('self', 'Self'), ('city', 'City'), ('logistics', 'Logistics')],
        'Delivery method')
    payment = fields.Boolean('Payment')

    @api.multi
    def action_check_credit(self):
        self.ensure_one()
        credit_line = self.partner_id.credit_line
        current_order_amount = self.amount_total
        receivable = self.partner_id.credit

        if credit_line < receivable + current_order_amount:
            raise Warning(_('Exceed credit line'))
        else:
            self.credit_check = True

    @api.multi
    def action_special_confirm(self):
        super(sale_order, self).action_confirm()

    @api.multi
    def action_confirm(self):

        if not self.stock:
            raise Warning(_('Stock was not enough'), )
        if not self.credit_check:
            raise Warning(_('Exceed credit line'), )
        if not self.payment:
            raise Warning(_('Not payment'))

        super(sale_order, self).action_confirm()
